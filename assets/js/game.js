/**
 * Created by Matthijs on 13-9-2017.
 */

var gameArray = ['A','A','B','B','C','C','D','D','E','E','F','F','G','G','H','H','I','I','J','J','K','K','L','L'];
var gameValues = [];
var gameKaartid = [];
var gameOmdraai = 0;

Array.prototype.gameShuffle = function() {
    var i = this.length, j, temp;
    while(--i > 0){
        j = Math.floor(Math.random()*(i+1));
        temp = this[j];
        this[j]=this[i];
        this[i]= temp;
    }
}

function reset() {
    gameOmdraai = 0;
    var output = '';
    gameArray.gameShuffle();
    for(var i =0; i< gameArray.length; i++){
        output += '<div class="rounded kaart" id="kaart_'+i+'" onclick="gameDraaiKaart(this,\''+gameArray[i]+'\')"></div>';
    }
    document.getElementById('game').innerHTML = output;
}

function gameDraaiKaart(tile, val) {

    if (tile.innerHTML == "" && gameValues.length < 2) {
        tile.style.background = '#FFF';
        tile.innerHTML = val;

        if (gameValues.length == 0) {
            gameValues.push(val);
            gameKaartid.push(tile.id);
        } else if (gameValues.length == 1) {
            gameValues.push(val);
            gameKaartid.push(tile.id);

            if (gameValues[0] == gameValues[1]) {
                gameOmdraai += 2;
                gameValues = [];
                gameKaartid = [];

                if (gameOmdraai == gameArray.length) {
                    alert("Gefeliciteerd! Er wordt een nieuw bord gegenereerd");
                    document.getElementById('game').innerHTML = "";
                    reset();
                }
            } else {
                function flipKaart() {
                    var kaart_1 = document.getElementById(gameKaartid[0]);
                    var kaart_2 = document.getElementById(gameKaartid[1]);
                    kaart_1.style.background='url(../img/elkarte.jpg) ';
                    kaart_1.innerHTML= "";
                    kaart_2.style.background='url(../img/elkarte.jpg) ';
                    kaart_2.innerHTML= "";
                    gameValues=[];
                    gameKaartid=[];
                }
                setTimeout(flipKaart, 700);
                kaart_1.style.background='url(../img/elkarte.jpg) ';
                kaart_2.style.background='url(../img/elkarte.jpg) ';
            }

        }
    }

}
